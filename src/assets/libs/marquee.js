import gsap from 'gsap';

class Marquee {
  constructor(el) {
    this.el = el;
    this.settings = {
      horizontal: true,
      vertical: true,
      speed: 300,
      container: this.el.parentNode,
    };
    this.containerWidth = this.settings.container.offsetWidth;
    this.containerHeight = this.settings.container.offsetHeight;
    this.elWidth = this.el.offsetWidth;
    this.elHeight = this.el.offsetHeight;
    this.bumpEdge = () => {
      const newColor = `hsl(${Math.floor(Math.random() * 360)}, 100%, 50%)`;
      gsap.set('.logo', { fill: newColor });
    };
  }

  init() {
    this.getSizes();

    gsap.set(this.el, {
      autoAlpha: 1,
      left: 0,
      top: 0,
      x: (this.elWidth / -2),
      y: (this.elHeight / -2),
    });

    if (this.settings.horizontal) {
      this.right();
    }
    if (this.settings.vertical) {
      this.down();
    }

    window.addEventListener('resize', this.getSizes.bind(this));
  }

  getSizes() {
    this.containerWidth = this.settings.container.offsetWidth;
    this.containerHeight = this.settings.container.offsetHeight;
    this.elWidth = this.el.offsetWidth;
    this.elHeight = this.el.offsetHeight;
  }

  right() {
    gsap.to(this.el, this.containerWidth / this.settings.speed, {
      x: this.containerWidth - this.elWidth,
      ease: gsap.Power0.easeNone,
      onComplete: () => {
        this.bumpEdge();
        this.left();
      },
    });
  }

  left() {
    gsap.to(this.el, this.containerWidth / this.settings.speed, {
      x: 0,
      ease: gsap.Power0.easeNone,
      onComplete: () => {
        this.bumpEdge();
        this.right();
      },
    });
  }

  down() {
    gsap.to(this.el, this.containerHeight / this.settings.speed, {
      y: this.containerHeight - this.elHeight,
      ease: gsap.Power0.easeNone,
      onComplete: () => {
        this.bumpEdge();
        this.up();
      },
    });
  }

  up() {
    gsap.to(this.el, this.containerHeight / this.settings.speed, {
      y: 0,
      ease: gsap.Power0.easeNone,
      onComplete: () => {
        this.bumpEdge();
        this.down();
      },
    });
  }

  destroy() {
    gsap.set(this.el, { autoAlpha: 0 });

    gsap.killAll();

    window.removeEventListener('resize', this.getSizes.bind(this));
  }
}

export default Marquee;
