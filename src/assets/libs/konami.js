class Konami {
  constructor() {
    this.konami = '38,38,40,40,37,39,37,39,66,65';
    this.keys = [];

    this.handleKeyDown = event => this.sequence(event);
  }

  init() {
    window.addEventListener('keydown', this.handleKeyDown);
  }

  sequence(event) {
    this.keys.push(event.keyCode);
    if (this.keys.toString().indexOf(this.konami) >= 0) {
      this.destroy();
      console.log('ale ale ale');
    }
  }

  destroy() {
    this.keys = [];
    window.removeEventListener('keydown', this.handleKeyDown);
  }
}

export default Konami;
